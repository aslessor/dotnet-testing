using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

using MEDIC_FHIR_API.Controllers;
using MEDIC_FHIR_API.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace MEDIC_FHIR_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            // services.AddControllersWithViews();
            services.AddSingleton<IHolidaysApiService, HolidaysApiService>();
            services.AddSingleton<IGithubService, GithubService>();
            services.AddSingleton<IFhirService, FhirService>();
            services.AddSingleton<IReportService, ReportService>();

            services.AddHttpClient("FhirTestApi", c => c.BaseAddress = new Uri("https://hapi-fhir.marc-hi.ca:8443/fhir/"));
            services.AddHttpClient("PublicHolidaysApi", c => c.BaseAddress = new Uri("https://date.nager.at"));
            services.AddHttpClient("GithubApi", c => c.BaseAddress = new Uri("https://api.github.com"));
            services.AddHttpClient("EasyLabsApi", c => c.BaseAddress = new Uri("https://api.easylabs.org"));

            // Set up dependency injection for controller's logger
            // services.AddScoped<ILogger, Logger<PatientController>>();
            // services.AddScoped<ILogger, Logger<PractitionerController>>();
            // // Set up dependency injection for services
            // services.AddScoped<IFHIRService, FHIRService>();
            // services.AddScoped<IPatientService, PatientService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI();

            // app.UseSwaggerUI(c =>
            // {
            //     c.SwaggerEndpoint("/swagger/v1/swagger.json", "MedicAPI");
            //     c.RoutePrefix = string.Empty;
            // });

            // if (env.IsDevelopment())
            // {
                app.UseDeveloperExceptionPage();
            // }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
