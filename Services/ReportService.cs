﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
using System.Text.Json;
// using System.Threading.Tasks;
using MEDIC_FHIR_API.Models;


// namespace EasyLabsMicroService.Services
namespace MEDIC_FHIR_API.Services
{
    public class ReportService : IReportService
    {
        private readonly HttpClient client;

        public ReportService(IHttpClientFactory clientFactory) {
            client = clientFactory.CreateClient("EasyLabsApi");
        }

        public async Task<Authenticate> GetToken() {
            var url = "/instances/authenticate";

            var result = new Authenticate();
            client.DefaultRequestHeaders.Add("client-id", "MlJDsCImxHOuhtNwC9");
            client.DefaultRequestHeaders.Add("secret-key", "2wgUw-F_50dkym=esKweUtn7LqW7t__jcMZm8HfedzSBCrxZiGf4^6qIYBrZTgK0iQ032gpLo-M3eORApIAf8F2y^40wM=tSO0TKoki3CfkLauRAvbF3YyKS");
            
            // client.DefaultRequestHeaders.Accept.Clear();
            // client.DefaultRequestHeaders.Accept.Add(
            //     new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            // client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

            // var response = await client.GetAsync(url);
            var response = client.GetStreamAsync(url);
            result = await JsonSerializer.DeserializeAsync<Authenticate>(await response);
            return result;
        }


    }
}




namespace MEDIC_FHIR_API.Services {
    public interface IReportService {
        Task<Authenticate> GetToken();
    }

}