using MEDIC_FHIR_API.Models;
using Hl7.Fhir.Model;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Text.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MEDIC_FHIR_API.Services {
    public interface IFhirService {
        Task<List<PatientModel>> AllPatients();
        Task<Patient> SinglePatient();

    }
}

namespace MEDIC_FHIR_API.Services {

    public class FhirService : IFhirService {
        private readonly HttpClient client;
        public FhirService(IHttpClientFactory clientFactory) {
            client = clientFactory.CreateClient("FhirTestApi");
        }


         public async Task<List<PatientModel>> AllPatients() {         
            var url = "Patient";
            var result = new List<PatientModel>();

            // var response = await client.GetAsync(url);
            var response = client.GetStreamAsync(url);
            result = await JsonSerializer.DeserializeAsync<List<PatientModel>>(await response);
            return result;
        }

         public async Task<Hl7.Fhir.Model.Patient> SinglePatient() {         
            var url = "Patient/227394";
            var result = new Patient();

            var response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode) {

                var content = response.Content.ReadAsStringAsync();
                result = JsonSerializer.Deserialize<Patient>(
                    await content 
                    // new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase }
                    );
            } else {
                throw new HttpRequestException(response.ReasonPhrase);
            }
            // var response = client.GetStreamAsync(url);
            // result = await JsonSerializer.DeserializeAsync<Patient>(await response);
            return result;
        }

        //  public async Task<Hl7.Fhir.Model.Patient> SinglePatient() {         
        //     var url = "Patient/227394";
        //     var result = new Patient();

        //     // var response = await client.GetAsync(url);
        //     var response = client.GetStreamAsync(url);
        //     result = await JsonSerializer.DeserializeAsync<Patient>(await response);
        //     return result;
        // }

    }
}

