using MEDIC_FHIR_API.Models;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Text.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MEDIC_FHIR_API.Services
{
    public class GithubService : IGithubService
    {
        private readonly HttpClient client;
        public GithubService(IHttpClientFactory clientFactory) {
            client = clientFactory.CreateClient("GithubApi");
        }
        // private static readonly HttpClient client;    
        // static GithubService() {
        //     client = new HttpClient() {
        //         BaseAddress = new Uri(
        //             "https://api.github.com")
        //     };
        // }


         public async Task<List<GithubModel>> GetGithub() {            
            var url = "orgs/dotnet/repos";
            var result = new List<GithubModel>();

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

            // var response = await client.GetAsync(url);
            var response = client.GetStreamAsync(url);
            result = await JsonSerializer.DeserializeAsync<List<GithubModel>>(await response);
            return result;
        }

        //  public async Task<List<GithubModel>> GetGithub() {            
        //     var url = "orgs/dotnet/repos";
        //     var result = new List<GithubModel>();

        //     var response = await client.GetAsync(url);
        //     if (response.IsSuccessStatusCode) {

        //         var stringResponse = await response.Content.ReadAsStringAsync();
        //         result = JsonSerializer.Deserialize<List<GithubModel>>(
        //             stringResponse, 
        //             new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
            
        //     } else {
        //         throw new HttpRequestException(response.ReasonPhrase);
        //     }

        //     return result;
        // }

    }
}


namespace MEDIC_FHIR_API.Services {
    public interface IGithubService {
        Task<List<GithubModel>> GetGithub();
    }

}

