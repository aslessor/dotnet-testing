// using System.Text.Json.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MEDIC_FHIR_API.Models;


    public class Practitioner
    {
        public DateTime Date { get; set; }
        public int TemperatureC { get; set; }
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
        public string? Summary { get; set; }
    }


    // public class Practitioner
    // {
    //     [JsonProperty("id")]
    //     /// <summary>
    //     /// unique identifier
    //     /// </summary>

    //     public string Id { get; set; }

    //     // [JsonProperty("resourceType")]
    //     // /// <summary>
    //     // /// resource type
    //     // /// </summary>
    //     // public string resourceType { get; set; }
    // }
