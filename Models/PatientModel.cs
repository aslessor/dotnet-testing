using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// namespace MEDIC_FHIR_API.Models
// {
//     public class AllPatients
//     {
//         public List<Item> Items { get; set; }
//     }

//     public class Item
//     {
//         public List<Weapon> Weapons { get; set; }
//     }

// }

namespace MEDIC_FHIR_API.Models
{
    public class PatientModel
    {
        [JsonProperty("id")]
        /// <summary>
        /// unique identifier
        /// </summary>
        public string Id { get; set; }

        // [JsonProperty("firstName")]
        // /// <summary>
        // /// first name
        // /// </summary>
        // public string FirstName { get; set; }

        // [JsonProperty("lastName")]
        // /// <summary>
        // /// last name
        // /// </summary>
        // public string LastName { get; set; }

        // [JsonProperty("dateOfBirth")]
        // /// <summary>
        // /// date of birth
        // /// </summary>
        // public string DateOfBirth { get; set; }

        // [JsonProperty("gender")]
        // /// <summary>
        // /// gender
        // /// </summary>
        // public string Gender { get; set; }

        // [JsonProperty("deceased")]

        // /// <summary>
        // /// Deceased
        // /// </summary>
        // public string Deceased { get; set; }

        // [JsonProperty("phone")]
        // /// <summary>
        // /// List of phone numbers
        // /// </summary>
        // public List<string> Telecom { get; set; }

        // [JsonProperty("address")]
        // /// <summary>
        // /// List of phone numbers
        // /// </summary>
        // public List<Address> Address { get; set; }

        // [JsonProperty("versionId")]
        // /// <summary>
        // /// Version id of the patient
        // /// </summary>
        // public string VersionId { get; set; }

        // [JsonProperty("maritalStatus")]
        // /// <summary>
        // /// marital status of the patient
        // /// </summary>
        // public string MaritalStatus { get; set; }
    }
}
