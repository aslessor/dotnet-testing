// using System.Text.Json.Serialization;
using System.Text.Json;
// using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace MEDIC_FHIR_API.Models;


    public class GithubModel {
        [JsonPropertyName("name")]
        public string? Name { get; set; }

        [JsonPropertyName("description")]
        public string? Description { get; set; }

        // [JsonPropertyName("html_url")]
        // public Uri GitHubHomeUrl { get; set; }

        // [JsonPropertyName("homepage")]
        // public Uri Homepage { get; set; }

        // [JsonPropertyName("watchers")]
        // public int Watchers { get; set; }
    }





