using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Hl7.Fhir.Model;


namespace MEDIC_FHIR_API.Models {


    public class MetaDatetime {
        public DateTime lastUpdated { get; set; }
    }

    public class AllPatientsModel {
    // "resourceType": "Bundle",
    public string? resourceType { get; set; }

    // "id": "a30c42db-9891-47a5-9863-1ccf3daf11eb",
    public string? Id { get; set; }

    // "meta": {
        // "lastUpdated": "2021-11-17T08:37:58.062-05:00"
    // },
    public string? meta { get; set; }
    
    // "type": "searchset",
    public string type { get; set; }

    // "total": 58,
    public int total { get; set; }

    // public IEnumerable<Hl7.Fhir.Model.Patient> Link { get; set; }

    // public IEnumerable<Hl7.Fhir.Model.Patient> Entry { get; set; }

 
    }
}
