﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MEDIC_FHIR_API.Models
{
    public class Authenticate
    {
        [JsonPropertyName("status")]
        public bool Status { get; set; }
        
        [JsonPropertyName("status_code")]
        public int StatusCode { get; set; }

        [JsonPropertyName("token")]
        public string? Token { get; set; }
    }

}
