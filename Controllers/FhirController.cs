using MEDIC_FHIR_API.Models;
using MEDIC_FHIR_API.Services;

using Hl7.Fhir.Model;


using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;


namespace MEDIC_FHIR_API.Controllers;

    [ApiController]
    [Route("fhir")]
    public class FhirController : Controller {
        private readonly IFhirService _fhirService;        
        public FhirController(IFhirService fhirService) {
            _fhirService = fhirService;
        } 


        [HttpGet, Route("/read_all_patients")]
        public async Task<ActionResult<IEnumerable<PatientModel>>> GetFhirIndex() {
            List<PatientModel> patient = new List<PatientModel>();
            patient = await _fhirService.AllPatients();
            return patient;    
        }

        [HttpGet, Route("/read_single_patient")]
        public async Task<ActionResult<Hl7.Fhir.Model.Patient>> GetSinglePatient() {
            Hl7.Fhir.Model.Patient patient = new Hl7.Fhir.Model.Patient();
            patient = await _fhirService.SinglePatient();
            return patient;    
        }

        
    }


