﻿using MEDIC_FHIR_API.Models;
using MEDIC_FHIR_API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace MEDIC_FHIR_API.Controllers
{
    [ApiController]
    [Route("easylabs")]
    public class ReportController : Controller

    {
        // private readonly ILogger<ReportController> _logger;
        // private readonly IEasyLabsService _easylabsService;
        // private static HttpClient client;
        // public ReportController(ILogger<ReportController> logger, IEasyLabsService easylabsService) {
            // _logger = logger;
            // _easylabsService = easylabsService;
        // }
        private readonly IReportService _reportService;
        public ReportController(IReportService reportService) {
            _reportService = reportService;
        }



        [HttpPost, Route("/reports/generate")] // renders to reports/generate endpoint
        public async Task<Authenticate> PostReport() 
        {
            // Authenticate tok = new Authenticate();
            var tok = await _reportService.GetToken();
            return tok;  
        }

            // 1. authenticate eastylabs and get token
                    // - save that token
            // 2. Write logic:
                //  - check if token expired.
                //  - if token is expired
                //      - generate new token
                //  - elif token not expired 
                //      - use existing token
                
            // Summary point:
                // - save token somewhere.
                // - re-use token for same user, unless its expired.
                // - is it possible to make http client and headers modular.




            // List<TestData> testDataList = new List<TestData>();
            // var baseURL = _easylabsService.GetBaseUrl();
            // var token = Environment.GetEnvironmentVariable("TOKEN");
            // client = new HttpClient();
            // client.BaseAddress = new Uri($"{baseURL}");
            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);


            // HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/reports/generate");
            // request.Content = new StringContent(payload.ToString(),
            //                         Encoding.UTF8,
            //                         "application/json");

            // HttpResponseMessage response = await client.SendAsync(request);
            // var content = await response.Content.ReadAsStringAsync();

            // try
            // {
            //     if (!response.IsSuccessStatusCode){
            //         return StatusCode(400);
            //     }

            //     return StatusCode(200, content);
            // }
            // catch (Exception )
            // {
            //     return StatusCode(StatusCodes.Status500InternalServerError);
            // }
        // }


        // [HttpGet("/reports")] // renders to /reports?user_id={ user_id}&report_id={report_id}
        // public async Task<IActionResult> GetReport(string user_id, string report_id)
        // {
        //     var client_id = _easylabsService.GetClientId();
        //     var key = _easylabsService.GetSecretKey();
        //     var baseURL = _easylabsService.GetBaseUrl();

        //     var token = Environment.GetEnvironmentVariable("TOKEN");
        //     HttpClient client = new HttpClient();
        //     client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        //     var request = new HttpRequestMessage(HttpMethod.Get,
        //                 $"{baseURL}/reports?user_id={user_id}&report_id={report_id}");


        //     HttpResponseMessage response = await client.SendAsync(request);
        //     var content = await response.Content.ReadAsStringAsync();

        //     if (string.IsNullOrEmpty(user_id) || string.IsNullOrEmpty(report_id) || !response.IsSuccessStatusCode){
        //         return StatusCode(400);
        //     }
        //     return  StatusCode(200, content);
        // }

    }
}
