using MEDIC_FHIR_API.Models;
using MEDIC_FHIR_API.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
// using Newtonsoft.Json;

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;

// using RestSharp;

// using System.Net.Http;
// using System.Net.Http.Headers;


namespace MEDIC_FHIR_API.Controllers;
// {

    [ApiController]
    [Route("github")]
    public class GithubController : Controller
    {
        private readonly IGithubService _githubService;
        
        public GithubController(IGithubService githubService) {
            _githubService = githubService;
        } 

        // [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet, Route("/read_github")]
        public async Task<ActionResult<IEnumerable<GithubModel>>> GetGithubIndex() {
            List<GithubModel> github = new List<GithubModel>();
            github = await _githubService.GetGithub();
            return github;    
        }

        
    }

// }