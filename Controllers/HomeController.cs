using MEDIC_FHIR_API.Models;
using MEDIC_FHIR_API.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;

using RestSharp;

using System.Net.Http;
using System.Net.Http.Headers;


namespace MEDIC_FHIR_API.Controllers;

    [ApiController]
    [Route("home")]
    public class HomeController : Controller
    {
        private readonly IHolidaysApiService _holidaysApiService;
        
        public HomeController(IHolidaysApiService holidaysApiService) {
            _holidaysApiService = holidaysApiService;
        } 

        [HttpGet, Route("/read_home")]
        public async Task<IActionResult> Index(string countryCode, int year) {
            List<HolidayModel> holidays = new List<HolidayModel>();
            holidays = await _holidaysApiService.GetHolidays(countryCode, year);
            return View(holidays);
        }
    }


