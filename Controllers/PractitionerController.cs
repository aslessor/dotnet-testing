using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

using MEDIC_FHIR_API.Models;
using MEDIC_FHIR_API.Services;

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;

using RestSharp;

using System.Net.Http;
using System.Net.Http.Headers;

// using Hl7.Fhir.Model;

namespace MEDIC_FHIR_API.Controllers;
// {
    [ApiController]
    [Route("practitioner")]
    public class PractitionerController
    {
        private static readonly HttpClient client = new HttpClient();

        private static readonly string[] data = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };


        private readonly ILogger<PractitionerController> _logger;

        public PractitionerController(ILogger<PractitionerController> logger)
        {
            _logger = logger;
        }


        [HttpGet, Route("/read_one")]
        public IEnumerable<Practitioner> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new Practitioner
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = data[Random.Shared.Next(data.Length)]
            })
            .ToArray();
        }


        [HttpGet, Route("/read_two")]
        public IEnumerable<Practitioner> GetTwo()
        {
            return Enumerable.Range(1, 5).Select(index => new Practitioner
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = data[Random.Shared.Next(data.Length)]
            })
            .ToArray();
        }

        // [HttpGet, Route("/read_three")]
        // public async Task<IActionResult> GetThree()
        // {
        //     var arr = new List<IActionResult>();

        //     var client = new RestClient("https://api.github.com/orgs/dotnet/repos");
        //     var request = new RestRequest(Method.GET);

        //     var resp = await client.GetAsync(request);

        //     // IRestResponse response = await client.ExecuteAsync(request);
            
        //     // arr = JsonConvert.DeserializeObject(response.Content);
        //     // return content;


        //     // Console.WriteLine(response.Content);
        //     // return StatusCode(200, 
        //                     // JsonConvert.SerializeObject(response.Content,
        //                                                 // Formatting.Indented,
        //                                                 // new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
        //     // arr.Add(response.Content);
        //     return arr;
        // }

        // [HttpGet("/github", Name='github')]
        // public static async Task ProcessRepositories()
        // {
        //     client.DefaultRequestHeaders.Accept.Clear();
        //     client.DefaultRequestHeaders.Accept.Add(
        //         new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
        //     client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

        //     var stringTask = client.GetStringAsync("https://api.github.com/orgs/dotnet/repos");

        //     var msg = await stringTask;
        //     Console.Write(msg);
        // }



        // [HttpGet(Name = "GetRanking")]
        // public static async Task ProcessRepositories()
        // {
        //     client.DefaultRequestHeaders.Accept.Clear();
        //     client.DefaultRequestHeaders.Accept.Add(
        //         new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
        //     client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

        //     var stringTask = client.GetStringAsync("https://api.github.com/orgs/dotnet/repos");

        //     var msg = await stringTask;
        //     Console.Write(msg);
        // }

        // [HttpGet("/read/practitioner", Name = "GetRanking")]
        // public async Task<IActionResult> GetAync()
        // {
        //     var client = new RestClient("https://api.football-data.org/v2/matches");
        //     var request = new RestRequest(Method.GET);
        //     IRestResponse response = await client.ExecuteAsync(request);


        //     // return Ok(response.Content);
        // }

        // public IPractitionerController
        // {
            // repository = new HttpClient();
        // }

        // private readonly InMemItemsRepository repository;

        // public IPractitionerController()
        // {
        //     repository = new InMemItemsRepository();
        // }

        // [HttpGet]
        // public IEnumerable<Item> GetItems() {
            // var items = repository.GetItems();
            // return '';
        // }

        // [HttpGet("{id}")]
        // public Item GetItem(Guid id) {
        //     var item = repository.GetItem(id);
        //     return item;
        // }

    }
// }